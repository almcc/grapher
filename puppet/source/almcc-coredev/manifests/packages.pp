# Class: coredev::packages
#
#
class coredev::packages {
    require coredev::repos

    package { ['tree',
               'createrepo',
               'htop',
               'ruby-devel',
               'tidy',
               'tmux',
               'multitail',
               'python-setuptools'
              ]:
        ensure => installed,
    }

    #  fpm does not seem to be cleaver enought to set
    #  it's dependecies as rpm dependencies when making
    #  an rpm of itself so you have to specifiy them all.
    package { ['rubygem-fpm',
               'rubygem-json',
               'rubygem-arr-pm',
               'rubygem-cabin',
               'rubygem-childprocess',
               'rubygem-backports',
               'rubygem-clamp',
               'rubygem-ffi']:
        ensure => installed,
    }
}
