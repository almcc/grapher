# Class: coredev::python
#
#
class coredev::python {
    require coredev::repos

    package { 'python27':
        ensure => '2.7.9-1',
    }

    package { 'python27-pip':
        ensure => '6.0.8-1',
        require => Package['python27'],
    }

    package { 'python27-setuptools':
        ensure => '12.0.5-1',
        require => Package['python27'],
    }

    exec { 'pip-symlink':
        command      => 'ln -s /usr/local/bin/pip /usr/bin/pip-python',
        path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
        creates => '/usr/bin/pip-python',
        require => Package['python27-pip'],
    }
}