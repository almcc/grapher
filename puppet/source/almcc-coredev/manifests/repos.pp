class coredev::repos {

  file { "CentOS-Base.repo":
    path => "/etc/yum.repos.d/CentOS-Base.repo",
    ensure => absent,
  }

  file { "CentOS-Debuginfo.repo":
    path => "/etc/yum.repos.d/CentOS-Debuginfo.repo",
    ensure => absent,
  }

  file { "CentOS-Media.repo":
    path => "/etc/yum.repos.d/CentOS-Media.repo",
    ensure => absent,
  }

  file { "CentOS-Vault.repo":
    path => "/etc/yum.repos.d/CentOS-Vault.repo",
    ensure => absent,
  }

  file { "puppetlabs.repo":
    path => "/etc/yum.repos.d/puppetlabs.repo",
    ensure => absent,
  }

  file { "epel-testing.repo":
    path => "/etc/yum.repos.d/epel-testing.repo",
    ensure => absent,
  }

  yumrepo { 'base':
    descr          => 'CentOS-$releasever - Base',
    baseurl        => 'http://mirror.bytemark.co.uk/centos/$releasever/os/$basearch/',
    enabled        => 1,
    gpgcheck       => 0,
    require        => [
      File['CentOS-Base.repo'],
      File['CentOS-Debuginfo.repo'],
      File['CentOS-Media.repo'],
      File['CentOS-Vault.repo'],
      File['puppetlabs.repo'],
      File['epel-testing.repo']
    ]
  }

  yumrepo { 'updates':
    descr          => 'CentOS-$releasever - Updates',
    baseurl        => 'http://mirror.bytemark.co.uk/centos/$releasever/updates/$basearch/',
    enabled        => 1,
    gpgcheck       => 0,
    require        => [
      File['CentOS-Base.repo'],
      File['CentOS-Debuginfo.repo'],
      File['CentOS-Media.repo'],
      File['CentOS-Vault.repo'],
      File['puppetlabs.repo'],
      File['epel-testing.repo']
    ]
  }

  yumrepo { 'extras':
    descr          => 'CentOS-$releasever - Extras',
    baseurl        => 'http://mirror.bytemark.co.uk/centos/$releasever/extras/$basearch/',
    enabled        => 1,
    gpgcheck       => 0,
    require        => [
      File['CentOS-Base.repo'],
      File['CentOS-Debuginfo.repo'],
      File['CentOS-Media.repo'],
      File['CentOS-Vault.repo'],
      File['puppetlabs.repo'],
      File['epel-testing.repo']
    ]
  }

  yumrepo { 'centosplus':
    descr          => 'CentOS-$releasever - Centosplus',
    baseurl        => 'http://mirror.bytemark.co.uk/centos/$releasever/centosplus/$basearch/',
    enabled        => 1,
    gpgcheck       => 0,
    require        => [
      File['CentOS-Base.repo'],
      File['CentOS-Debuginfo.repo'],
      File['CentOS-Media.repo'],
      File['CentOS-Vault.repo'],
      File['puppetlabs.repo'],
      File['epel-testing.repo']
    ]
  }

  yumrepo { 'contrib':
    descr          => 'CentOS-$releasever - Contrib',
    baseurl        => 'http://mirror.bytemark.co.uk/centos/$releasever/contrib/$basearch/',
    enabled        => 1,
    gpgcheck       => 0,
    require        => [
      File['CentOS-Base.repo'],
      File['CentOS-Debuginfo.repo'],
      File['CentOS-Media.repo'],
      File['CentOS-Vault.repo'],
      File['puppetlabs.repo'],
      File['epel-testing.repo']
    ]
  }

  yumrepo { 'epel':
    descr          => 'Epel - CentOS 6',
    baseurl        => 'http://mirror.bytemark.co.uk/fedora/epel/6/$basearch/',
    enabled        => 1,
    gpgcheck       => 0,
    require        => [
      File['CentOS-Base.repo'],
      File['CentOS-Debuginfo.repo'],
      File['CentOS-Media.repo'],
      File['CentOS-Vault.repo'],
      File['puppetlabs.repo'],
      File['epel-testing.repo']
    ]
  }

  yumrepo { 'rpmforge':
    descr          => 'Rpmforge - el6',
    baseurl        => 'http://apt.sw.be/redhat/el6/en/$basearch/rpmforge/',
    enabled        => 1,
    gpgcheck       => 0,
    require        => [
      File['CentOS-Base.repo'],
      File['CentOS-Debuginfo.repo'],
      File['CentOS-Media.repo'],
      File['CentOS-Vault.repo'],
      File['puppetlabs.repo'],
      File['epel-testing.repo']
    ]
  }

  yumrepo { 'puppetlabs-products':
    descr          => 'Pupppetlabs - el6 - Products',
    baseurl        => 'http://yum.puppetlabs.com/el/6/products/$basearch/',
    enabled        => 1,
    gpgcheck       => 0,
    require        => [
      File['CentOS-Base.repo'],
      File['CentOS-Debuginfo.repo'],
      File['CentOS-Media.repo'],
      File['CentOS-Vault.repo'],
      File['puppetlabs.repo'],
      File['epel-testing.repo']
    ]
  }

  yumrepo { 'puppetlabs-dependencies':
    descr          => 'Pupppetlabs - el6 - Dependencies',
    baseurl        => 'http://yum.puppetlabs.com/el/6/dependencies/$basearch/',
    enabled        => 1,
    gpgcheck       => 0,
    require        => [
      File['CentOS-Base.repo'],
      File['CentOS-Debuginfo.repo'],
      File['CentOS-Media.repo'],
      File['CentOS-Vault.repo'],
      File['puppetlabs.repo'],
      File['epel-testing.repo']
    ]
  }

  yumrepo { 'vender-rpms':
    descr          => 'Vender Rpms',
    baseurl        => 'file:///home/vagrant/project/vendor/rpms',
    enabled        => 1,
    gpgcheck       => 0,
    require        => [
      File['CentOS-Base.repo'],
      File['CentOS-Debuginfo.repo'],
      File['CentOS-Media.repo'],
      File['CentOS-Vault.repo'],
      File['puppetlabs.repo'],
      File['epel-testing.repo']
    ]
  }

}