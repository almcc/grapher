# == Class: modelstore
#
# Full description of class modelstore here.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { modelstore:
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class modelstore {
    require coredev::repos
    require coredev::python

    package { 'qpid-cpp-server':
        ensure => installed,
    }

    service { 'qpidd':
        enable => true,
        ensure => running,
        hasrestart => true,
        hasstatus => true,
        require => Package["qpid-cpp-server"],
    }

    package { 'qpid-tools':
        ensure => installed,
    }

    package { 'qpid-python':
        provider => pip,
        ensure => '0.26.1',
    }

    package { 'django':
        provider => pip,
        ensure => '1.7.4',
    }

    package { 'djangorestframework':
        provider => pip,
        ensure => '3.0.4',
    }

    package { 'django-cors-headers':
        provider => pip,
        ensure => '1.0.0',
    }

    package { 'rest_framework_ember':
        provider => pip,
        ensure => '1.2.0',
    }

    package { 'markdown':
        provider => pip,
        ensure => '2.5.2',
    }

    package { 'django-filter':
        provider => pip,
        ensure => '0.9.2',
    }

    package { 'httpd':
        ensure => '2.2.15-39.el6.centos',
    }

    package { 'mod_wsgi':
        ensure => '3.2-7.el6',
    }

    service { 'httpd':
        enable      => true,
        ensure      => running,
        hasrestart => true,
        hasstatus  => true,
        require    => [Package["httpd"], Package["mod_wsgi"]],
    }
}
