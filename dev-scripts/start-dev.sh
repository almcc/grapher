#!/bin/sh
tmux new-session -d -s 'grapher' 'htop'
tmux new-window -t grapher -n 'bridge' 'cd ~/project/source/bridge/; python serve.py'
tmux new-window -t grapher -n 'modelstore' 'cd ~/project/source/modelstore/; python manage.py runserver 0.0.0.0:8001'
tmux new-window -t grapher -n 'logic' 'cd ~/project/source/logic/; python logic.py'
tmux new-window -t grapher -n 'ui' 'cd ~/project/source/ui/; python -m SimpleHTTPServer 8003'
