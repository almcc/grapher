CentOS 6 Vagrant Development Environment
========================================

This is a CentOS 6 development environment that only uses RPM packages, it includes fpm and a locate yum repo (in adition to epel and rpmforge) for packages.

Repo
----

The repo is in vendor/rpms/. When you add and rpm, make sure to run createrepo before commiting it.

Puppet
------

Puppet packages are held in puppet/source/ with the manifest in puppet/mainfest/. The Makefile at puppet/source/Makefile will build the package for each puppet module and install it in puppet/modules, this is the path that vagrant up when provisioning. The vagrant provision system will call this script before running puppet.

Home
----

This whole directroy is mounted at ~/project for easy access.
