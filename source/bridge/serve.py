from gevent import monkey; monkey.patch_all()
import gevent

from socketio import socketio_manage
from socketio.server import SocketIOServer
from socketio.namespace import BaseNamespace
from socketio.mixins import BroadcastMixin
from qpid.messaging import *
from qpid.util import URL
from qpid.log import enable, WARN

enable("qpid", WARN)

class AMQPNamespace(BaseNamespace, BroadcastMixin):

    def __init__(self, *args, **kwargs):
        super(AMQPNamespace, self).__init__(*args, **kwargs)
        self.qpid_connection = Connection('localhost',
                                           reconnect=True,
                                           reconnect_interval=3.0,
                                           reconnect_limit=0)
        self.qpid_connection.open()
        self.qpid_session = self.qpid_connection.session()

    def process_event(self, packet):
        args = packet['args']
        name = packet['name']

        snd = self.qpid_session.sender('amq.topic')
        msg = Message(subject=name,
                      reply_to=None,
                      content=args[0])
        snd.send(msg)

    def recv_connect(self):
        def send_message():
            try:
              rcv = self.qpid_session.receiver('amq.topic')
              while True:
                try:
                  msg = rcv.fetch(timeout=None)
                  self.emit(msg.subject, msg.content)
                  self.qpid_session.acknowledge()
                except Empty:
                  break
            except ReceiverError, e:
              print e
            except KeyboardInterrupt:
              pass

        self.spawn(send_message)

class Application(object):
    def __init__(self):
        self.buffer = []

    def __call__(self, environ, start_response):
        path = environ['PATH_INFO'].strip('/') or 'index.html'

        if path.startswith('static/') or path == 'index.html':
            try:
                data = open(path).read()
            except Exception:
                return not_found(start_response)

            if path.endswith(".js"):
                content_type = "text/javascript"
            elif path.endswith(".css"):
                content_type = "text/css"
            elif path.endswith(".swf"):
                content_type = "application/x-shockwave-flash"
            else:
                content_type = "text/html"

            start_response('200 OK', [('Content-Type', content_type)])
            return [data]

        if path.startswith("socket.io"):
            socketio_manage(environ, {'/amq.topic': AMQPNamespace})
        else:
            return not_found(start_response)


def not_found(start_response):
    start_response('404 Not Found', [])
    return ['<h1>Not Found</h1>']


if __name__ == '__main__':
    print 'Listening on port http://0.0.0.0:8002 and on port 10843 (flash policy server)'
    SocketIOServer(('0.0.0.0', 8002), Application(),
        resource="socket.io", policy_server=True,
        policy_listener=('0.0.0.0', 10843)).serve_forever()
