App = Ember.Application.create();

function createGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}

App.Router.map(function() {
    this.resource('configuration', function(){
        this.resource('settings', function() {
            this.route('edit', { path: ':setting_id/edit'});
            this.route('create', { path: 'create'});
        });
        this.resource('setting', { path: 'setting/:setting_id'});
    });
    this.resource('graphs', function() {
        this.route('create', { path: 'create'});
    });
});

App.ApplicationAdapter = DS.RESTAdapter.extend({
  namespace: 'api/v1',
  host: 'http://localhost:8001',
  ajaxError: function(jqXHR) {
    var error = this._super(jqXHR);
    if (jqXHR && jqXHR.status === 400) {
      var response = Ember.$.parseJSON(jqXHR.responseText),
          errors = {},
          keys = Ember.keys(response);
      if (keys.length === 1) {
        var jsonErrors = response[keys[0]];
        Ember.EnumerableUtils.forEach(Ember.keys(jsonErrors), function(key) {
          errors[key] = jsonErrors[key];
        });
      }
      return new DS.InvalidError(errors);
    } else {
      return error;
    }
  }
});

App.Setting = DS.Model.extend({
    name: DS.attr('string'),
    value: DS.attr('string'),
});

App.SettingsRoute = Ember.Route.extend({
    model: function(){
        return this.store.find('setting');
    }
});

App.SettingRoute = Ember.Route.extend({
    model: function(params) {
        return this.store.find('setting', params.setting_id);
    }
});

App.SettingsEditController = Ember.ObjectController.extend({
    actions: {
        cancel: function() {
            this.get('model').rollback();
            this.transitionToRoute("settings");
        },

        submit: function() {
            var setting = this.get('model');
            var self = this;
            if(setting.get("isDirty")){
                setting.save().then(function() {
                    self.transitionToRoute("settings");
                }, function(response) {

                });
            }
            self.transitionToRoute("settings");
        },

        remove: function() {
            var setting = this.get('model');
            var self = this;
            setting.destroyRecord().then(function() {
                self.transitionToRoute("settings");
            });
        }
    },
});

App.SettingsCreateRoute = Ember.Route.extend({
    model: function() {
        return this.store.createRecord('setting');
    }
});


App.SettingsCreateController = Ember.ObjectController.extend({
    actions: {
        cancel: function() {
            this.get('model').deleteRecord();
            this.transitionToRoute("settings");
        },

        submit: function() {
            var setting = this.get('model');
            var self = this;
            setting.save().then(function() {
                self.transitionToRoute("settings");
            }, function(response) {

            });
        }
    },
});

App.GraphsCreateController = Ember.ArrayController.extend({
    name: null,
    messages: [],
    guid: null,
    showButtonEnabled: true,

    handle: function(data, subject) {
        var obj = JSON.parse(data);
        console.log(obj);
        var self = window.App.__container__.lookup('controller:GraphsCreate');
        self.get('messages').insertAt(0, obj);
    },

    reset: function() {
        this.set('name', null);
        this.get('messages').clear();
        this.set('guid', null);
        this.set('showButtonEnabled', true);
    },

    actions: {
        dismiss: function() {
            socket.removeListener('message.create.graph.'+this.guid+'.#', this.handle);
            this.reset();
            this.transitionToRoute("graphs");
        },

        submit: function() {
            var name = this.get('name');
            if(name){
                this.set('showButtonEnabled', false);
                this.set('guid', createGuid())
                var please = {};
                please.name = name.trim()
                socket.on('message.create.graph.'+this.guid+'.#', this.handle);
                socket.emit('please.create.graph.'+this.guid, JSON.stringify(please));
            }
        }
    },
});

var socket = io.connect('http://localhost:8002/amq.topic');
socket.$emit = qpid.emit;

App.initializer({
    name: "listen-setting",
    initialize: function(container, application) {
        var store = container.lookup('store:main');
        socket.on('model.setting.*.created', function(data, subject) {
            var obj = JSON.parse(data);
            setTimeout(function(){
                store.push('setting', obj.instance);
            }, 200);
        });
        socket.on('model.setting.*.updated', function(data, subject) {
            var obj = JSON.parse(data);
            store.push('setting', obj.instance);
        });
        socket.on('model.setting.*.deleted', function(data, subject) {
            var obj = JSON.parse(data);
            if(store.hasRecordForId('setting', obj.instance.id)){
                store.find('setting', obj.instance.id).then(function (setting) {
                    store.unloadRecord(setting);
                });
            }
        });
    }
});
