qpid = {
  check: function(filter, subject) {
      var pass = true;
      var filterParts = filter.split('.');
      var subjectParts = subject.split('.');

      // convert # to a * and add *'s so that
      // there is the same number of parts
      if(filterParts[filterParts.length - 1] == '#') {
          filterParts[filterParts.length - 1] = '*';
          var extras = subjectParts.length - filterParts.length
          for(var i = 0; i < extras; i++) {
              filterParts.push('*');
          }
      }

      // there should be the same number of parts
      if(filterParts.length != subjectParts.length){
          pass = false;
      } else {
          // Check that the parts are either equal
          // or that the filter part is a *
          var i = 0;
          while (i < subjectParts.length && pass == true) {
              var filterPart = filterParts[i];
              var subjectPart = subjectParts[i];
              if(filterPart != '*') {
                  if(filterPart != subjectPart){
                      pass = false;
                  }
              }
              i++;
          }
      }
      return pass;
  },

  emit: function (name) {
    if (!this.$events) {
      return false;
    }

    var handlers = []
    var filters = Object.getOwnPropertyNames(this.$events);
    for (i = 0; i < filters.length; i++) {
      if(qpid.check(filters[i], name)){
        handlers.push(this.$events[filters[i]]);
      }
    }

    if (handlers.lenght == 0) {
      return false;
    }

    for (i = 0; i < handlers.length; i++) {
      handler = handlers[i];
      var args = Array.prototype.slice.call(arguments, 1);

      if ('function' == typeof handler) {
        handler.apply(this, [args, name]);
      } else if (io.util.isArray(handler)) {
        var listeners = handler.slice();

        for (var i = 0, l = listeners.length; i < l; i++) {
          listeners[i].apply(this, [args, name]);
        }
      } else {
        return false;
      }
    }

    return true;
  }
}