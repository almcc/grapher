from django.contrib import admin
from graphs.models import Graph, Node, Edge

class GraphAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_filter = ('created_at', 'updated_at')
    search_fields = ('name',)
    readonly_fields = ('created_at', 'updated_at')
    fieldsets = [
        ('Graph', {
            'fields': ('name',)
        }),
        ('Change History', {
            'classes': ('collapse', ),
            'fields': ('created_at', 'updated_at')
        })
    ]

admin.site.register(Graph, GraphAdmin)


class NodeAdmin(admin.ModelAdmin):
    list_display = ('name', 'graph')
    list_filter = ('created_at', 'updated_at')
    search_fields = ('name', 'graph')
    readonly_fields = ('created_at', 'updated_at')
    fieldsets = [
        ('Node', {
            'fields': ('name', 'graph')
        }),
        ('Change History', {
            'classes': ('collapse', ),
            'fields': ('created_at', 'updated_at')
        })
    ]

admin.site.register(Node, NodeAdmin)


class EdgeAdmin(admin.ModelAdmin):
    list_display = ('name', 'source', 'target')
    list_filter = ('created_at', 'updated_at')
    search_fields = ('name', 'source', 'target')
    readonly_fields = ('created_at', 'updated_at')
    fieldsets = [
        ('Edge', {
            'fields': ('name', 'source', 'target')
        }),
        ('Change History', {
            'classes': ('collapse', ),
            'fields': ('created_at', 'updated_at')
        })
    ]

admin.site.register(Edge, EdgeAdmin)
