from django.db import models

class Graph(models.Model):
    name = models.CharField(max_length=250, blank=False)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    def __unicode__(self):
        return self.name

class Node(models.Model):
    name = models.CharField(max_length=250, blank=False)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    graph = models.ForeignKey(Graph, related_name='nodes')

    def __unicode__(self):
        return self.name

class Edge(models.Model):
    name = models.CharField(max_length=250, blank=False)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    source = models.ForeignKey(Node, related_name='sources')
    target = models.ForeignKey(Node, related_name='targets')

    def __unicode__(self):
        return self.name
