from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from graphs import views

urlpatterns = patterns('',
    url(r'^graphs$', views.GraphList.as_view(), name='graphs-list'),
    url(r'^graphs/(?P<pk>[0-9]+)$', views.GraphDetail.as_view(), name='graphs-detail'),
    url(r'^graphs/(?P<graph_id>\d+)/nodes$', views.GraphNodeList.as_view()),
    url(r'^graphs/(?P<graph_id>\d+)/nodes/(?P<node_id>\d+)/sources$', views.GraphNodeSourceList.as_view()),

    url(r'^nodes$', views.NodeList.as_view(), name='nodes-list'),
    url(r'^nodes/(?P<pk>[0-9]+)$', views.NodeDetail.as_view(), name='nodes-detail'),

    url(r'^edges$', views.EdgeList.as_view(), name='edges-list'),
    url(r'^edges/(?P<pk>[0-9]+)$', views.EdgeDetail.as_view(), name='edges-detail'),
)

urlpatterns = format_suffix_patterns(urlpatterns)
