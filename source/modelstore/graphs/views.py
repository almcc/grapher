from graphs.models import Graph, Node, Edge
from graphs.serializers import GraphSerializer, NodeSerializer, EdgeSerializer
from rest_framework import generics

class GraphList(generics.ListCreateAPIView):
    queryset = Graph.objects.all()
    serializer_class = GraphSerializer

class GraphDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Graph.objects.all()
    serializer_class = GraphSerializer

class GraphNodeList(generics.ListAPIView):
    serializer_class = NodeSerializer
    def get_queryset(self):
        graph_id = self.kwargs['graph_id']
        return Graph.objects.get(id=graph_id).nodes.all()

class GraphNodeSourceList(generics.ListAPIView):
    serializer_class = EdgeSerializer
    def get_queryset(self):
        graph_id = self.kwargs['graph_id']
        node_id = self.kwargs['node_id']
        return Graph.objects.get(id=graph_id).nodes.get(id=node_id).sources.all()

class NodeList(generics.ListCreateAPIView):
    queryset = Node.objects.all()
    serializer_class = NodeSerializer

class NodeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Node.objects.all()
    serializer_class = NodeSerializer

class EdgeList(generics.ListCreateAPIView):
    queryset = Edge.objects.all()
    serializer_class = EdgeSerializer

class EdgeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Edge.objects.all()
    serializer_class = EdgeSerializer