from django.forms import widgets
from rest_framework import serializers
from graphs.models import Graph, Node, Edge

class GraphSerializer(serializers.ModelSerializer):
    class Meta:
        model = Graph
        fields = ('id', 'name', 'nodes')

    def validate_name(self, value):
        if len(value) < 3:
            raise serializers.ValidationError("Name must be greater than 3 characters long.")
        return value

class NodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Node
        fields = ('id', 'name', 'graph', 'sources', 'targets')

    def validate_name(self, value):
        if len(value) < 3:
            raise serializers.ValidationError("Name must be greater than 3 characters long.")
        return value

class EdgeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Edge
        fields = ('id', 'name', 'source', 'target')

    def validate_name(self, value):
        if len(value) < 3:
            raise serializers.ValidationError("Name must be greater than 3 characters long.")
        return value