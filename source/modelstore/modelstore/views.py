from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'settings': reverse('settings-list', request=request, format=format),
        'graphs': reverse('graphs-list', request=request, format=format),
        'nodes': reverse('nodes-list', request=request, format=format),
        'edges': reverse('edges-list', request=request, format=format),
    })