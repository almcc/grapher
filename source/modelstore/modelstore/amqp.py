from configuration.models import Setting
from configuration.serializers import SettingSerializer
from django.db.models.signals import post_save, post_delete, m2m_changed
from django.dispatch import receiver
import qpid.messaging
import time
import logging
import json

debug = logging.getLogger('modelstore.amqp.debug')

def send(server, address, subject, reply_to, content):
    debug.info('Called send function ({}, {}, {}, {}, {})'.format(server, address, subject, reply_to, content))

    conn = qpid.messaging.Connection(server,
                      reconnect=True,
                      reconnect_interval=3.0,
                      reconnect_limit=0)
    conn.open()
    ssn = conn.session()
    snd = ssn.sender(address)

    msg = qpid.messaging.Message(subject=subject,
                  reply_to=reply_to,
                  content=json.dumps(content))
    snd.send(msg)
    conn.close()

def broadcast(subject, content):
    send('localhost', 'amq.topic', subject, 'core', content)

def postSaveModel(sender, serializer, created):
    action = 'updated'
    if created == True:
        action = 'created'

    event = {}
    event['action'] = action
    event['instance'] = serializer.data
    event['model'] = sender.__name__

    subject = 'model.{}.{}.{}'.format(sender.__name__.lower(), serializer.data['id'], action)
    broadcast(subject, event)

def postDeleteModel(sender, serializer):
    event = {}
    event['action'] = 'deleted'
    event['instance'] = serializer.data
    event['model'] = sender.__name__
    subject = 'model.{}.{}.deleted'.format(sender.__name__.lower(), serializer.data['id'])
    broadcast(subject, event)

@receiver(post_save, sender=Setting)
def postSaveSettingHandler(sender, **kwargs):
    serializer = SettingSerializer(kwargs['instance'])
    postSaveModel(sender, serializer, kwargs['created'])

@receiver(post_delete, sender=Setting)
def postDeleteSettingHandler(sender, **kwargs):
    serializer = SettingSerializer(kwargs['instance'])
    postDeleteModel(sender, serializer)
