from django.conf.urls import patterns, include, url
from django.contrib import admin
import modelstore.amqp # Import so the hooks get regestered.
from modelstore import views

urlpatterns = patterns('',
    url(r'^api/v1/$', views.api_root),
    url(r'^api/v1/', include('configuration.urls')),
    url(r'^api/v1/', include('graphs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
