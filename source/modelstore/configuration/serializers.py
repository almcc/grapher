from django.forms import widgets
from rest_framework import serializers
from configuration.models import Setting

class SettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Setting
        fields = ('id', 'name', 'value')

    def validate_name(self, value):
        if len(value) < 3:
            raise serializers.ValidationError("Name must be greater than 3 characters long.")
        return value