from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from configuration import views

urlpatterns = patterns('',
    url(r'^settings$', views.SettingList.as_view(), name='settings-list'),
    url(r'^settings/(?P<pk>[0-9]+)$', views.SettingDetail.as_view(), name='settings-detail'),
)

urlpatterns = format_suffix_patterns(urlpatterns)
