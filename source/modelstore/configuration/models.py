from django.db import models

class Setting(models.Model):
    name = models.CharField(max_length=250, blank=False)
    value = models.CharField(max_length=250, blank=False)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    def __unicode__(self):
        return self.name
