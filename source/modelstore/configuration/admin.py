from django.contrib import admin
from configuration.models import Setting

class SettingAdmin(admin.ModelAdmin):
    list_display = ('name', 'value')
    list_filter = ('created_at', 'updated_at')
    search_fields = ('name', 'value')
    readonly_fields = ('created_at', 'updated_at')
    fieldsets = [
        ('Setting', {
            'fields': ('name', 'value')
        }),
        ('Change History', {
            'classes': ('collapse', ),
            'fields': ('created_at', 'updated_at')
        })
    ]

admin.site.register(Setting, SettingAdmin)
