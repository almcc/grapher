#!/usr/bin/env python
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import optparse
from qpid.messaging import *
from qpid.util import URL
import time
import json

conn = Connection("localhost",
                  reconnect=True,
                  reconnect_interval=3.0,
                  reconnect_limit=0)
try:
  conn.open()
  ssn = conn.session()
  rcv = ssn.receiver("amq.topic/please.create.graph.*.#")

  while True:
    try:
      revc_msg = rcv.fetch(timeout=None)
      guid = revc_msg.subject.split('.')[3]
      snd = ssn.sender('amq.topic')
      for i in range(10):
        content = {}
        content['message'] = 'Replying {}'.format(i)
        content['level'] = 'alert'
        status_msg = Message(subject='message.create.graph.{}.status'.format(guid),
                        reply_to=None,
                        content=json.dumps(content))
        snd.send(status_msg)
        time.sleep(1)

      content = {}
      content['message'] = 'Final'
      content['level'] = 'alert'
      final_msg = Message(subject='message.create.graph.{}.final'.format(guid),
                        reply_to=None,
                        content=json.dumps(content))
      snd.send(final_msg)


      ssn.acknowledge()
    except Empty:
      break
except ReceiverError, e:
  print e
except KeyboardInterrupt:
  pass

conn.close()
