import requests
import json


class API(object):
    def __init__(self, base):
        self._base = base
        self._parts = []
        self._should_get_list = False

    def __getattr__(self, resource):
        self._should_get_list = True
        self._parts.append(resource)
        return self

    def reset(self):
        self._parts = []
        self._should_get_list = False

    def id(self, obj_id):
        self._should_get_list = False
        self._parts.append(str(obj_id))
        return self

    def get(self, resource):
        response = None
        if self._should_get_list == True:
            response = self._get_list(resource)
        else:
            response = self._get_object(resource)
        self.reset()
        return response

    def _get_list(self, resource):
        response = self._fetch()
        if response.status_code == 200:
            return response.json()[resource]
        return None

    def _get_object(self, resource):
        response = self._fetch()
        if response.status_code == 200:
            return response.json()[resource]
        return None

    def _fetch(self):
        url = '{}{}'.format(self._base, '/'.join(self._parts))
        return requests.get(url)

    def save(self, resource, object):
        response = None
        if 'id' in object:
            self.id(object['id'])
            response = self._update(resource, object)
        else:
            response = self._create(resource, object)
        self.reset()
        return response

    def _update(self, resource, object):
        url = '{}{}'.format(self._base, '/'.join(self._parts))
        data = json.dumps({resource: object})
        headers = {'content-type': 'application/json; charset=UTF-8'}
        r = requests.put(url, data=data, headers=headers)
        print r.text
        if r.status_code == 200:
            return r.json()[resource]
        return None

    def _create(self, resource, object):
        url = '{}{}'.format(self._base, '/'.join(self._parts))
        data = json.dumps({resource: object})
        headers = {'content-type': 'application/json; charset=UTF-8'}
        r = requests.post(url, data=data, headers=headers)
        if r.status_code == 201:
            return r.json()[resource]
        return None

    def delete(self, resource, object):
        response = False
        self.id(object['id'])
        url = '{}{}'.format(self._base, '/'.join(self._parts))
        r = requests.delete(url)
        if r.status_code == 204:
            response = True
        self.reset()
        return response

if __name__ == "__main__":
    api = API('http://localhost:8001/api/v1/')
    print api.setting.get("setting")
    print api.graphs.id(1).get("graph")
    print api.graphs.id(1).nodes.id(1).sources.get("edge")

    # print api.nodes.ids([1, 2, 3]).get("node")  # does not work yet
    # print api.nodes.id(1).switch.id().get("switch")  # does not work yet
    # print api.nodes.filter(foo=bar).get("node")  # does not work yet
    # print api.nodes.filter(foo=bar).filter(bob=jane).get("node")  # does not work yet

    new = api.settings.save("setting", {'name': 'test', 'value':'test'})
    new['value'] = 'new'
    updated = api.settings.save("setting", new)
    print api.settings.delete("setting", updated)
